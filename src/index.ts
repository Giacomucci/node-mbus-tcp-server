var ModbusRTU = require('modbus-serial')

class Program {

    private _seconds: number = 0;
    private _serverTcp: any;
    private _vector: any;

    public constructor() {
        let that = this;
        this._vector = {
            getInputRegister: function (addr, unitID): any { return addr; },
            getHoldingRegister: function (addr, unitID) { if(addr == 4)return ++that._seconds; },
            getCoil: function (addr, unitID) { return (addr % 2) === 0; },
            setRegister: function (addr, value, unitID) { console.log('set register', addr, value, unitID); return; },
            setCoil: function (addr, value, unitID) { console.log('set coil', addr, value, unitID); return; }
        };
        this._serverTcp = new ModbusRTU.ServerTCP(this._vector, { host: "0.0.0.0", port: 502, unitID: 1 });        
    }
}

var program = new Program();